# DP Sports page

## Descripcion

Esta es una pagina para compra online de articulos deportivos


### Dependencias

Este projecto usa bootstrap 5.0.0, Ajax, JQuery


## Authors

Oscar Julian Toro Y Sergio Ivan Quintero

## References

Fuentes de las plantillas utilizadads: 
- https://w3layouts.com
- https://bootsnipp.com



## Acknowledgments

* [BootStrap](https://github.com/twbs/bootstrap)
* [Font-Awesome](https://github.com/FortAwesome/Font-Awesome)

![image](images/umlogo.png)